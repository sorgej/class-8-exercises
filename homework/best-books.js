// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
const formEl = document.getElementsByTagName('form')[0];
//console.log(submitEl);
formEl.addEventListener('submit', function(e) {
    e.preventDefault();

    //     A) Takes the user submission for year, month, and date.  You can assume that the input is valid.

    //on submit, event listener user input to variables
    let year;
    let month;
    let day;

    year = document.getElementsByName('year')[0].value;
	month = document.getElementsByName('month')[0].value;
	day = document.getElementsByName('date')[0].value;
    
    console.log(`${year}, ${month}, ${day}`);
    // store date for api search
    const date = `${year}-${month}-${day}`;

    // input date matches ^(\d{4}-\d{2}-\d{2}|current)$
    //YYYY-MM-DD or "current
    // Resets the input value
    //inputs.value = '';

    //     B) Calls the New York Times API to find the best selling hardcover fiction books for that date
    // use new variables to query selected input
    // find ny times api for querying these features

    const list = 'hardcover-fiction'
    const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json';
    const BASE_URL_2 = 
    `https://api.nytimes.com/svc/books/v3/lists/${date}/${list}.json`;
    //'https://api.nytimes.com/svc/search/v2/articlesearch.json';

    const url = `${BASE_URL_2}?&api-key=${API_KEY}`;

    fetch(url)
    .then(function(response) {
        return response.json();
    })
    .then(function(responseJson) {
        console.log(responseJson);

        window.responseJson = responseJson;  //?

        let books = responseJson.results.books;
        console.log(books);

     //     C) Displays the following items for the first 5 books:
        // looks at first 5 hardcover fiction for date
        // displays Title, Author & Description

        let titles = [];
        let authors = [];
        let descriptions = [];

        // store first 5 books for date
        for (let i = 0; i < 5; i++) {
            titles.push(books[i].title);
            authors.push(books[i].author);
            descriptions.push(books[i].description);
        };

        // create first book html
        // document.getElementById('books-title').innerText = titles[0];
        // document.getElementById('books-author').innerText = authors[0];
        // document.getElementById('books-desc').innerText = descriptions[0];

        const booksContainer = document.getElementById('books-container');
        //let container = document.getElementsByClassName('book');

        // append same initial html, replace inner text
        for (let i = 0; i < 5; i++) {
            //console.log(i);
            //create HTML for each subsequent book
            const divEl = document.createElement('div');
            const titleEl = document.createElement('p');
            const titleText = document.createTextNode(titles[i]);

            const authorEl = document.createElement('p');
            const authorText = document.createTextNode(authors[i]);
            const descriptionEl = document.createElement('p');
            const descriptionText = document.createTextNode(descriptions[i]);

            //append text nodes
            titleEl.appendChild(titleText);
            authorEl.appendChild(authorText);
            descriptionEl.appendChild(descriptionText);
            //append HTML structure
            divEl.appendChild(titleEl);
            //console.log(divEl);

            divEl.appendChild(authorEl);
            divEl.appendChild(descriptionEl);
            //console.log(divEl);
            booksContainer.appendChild(divEl);
        };
    });
});
