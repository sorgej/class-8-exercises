let myPromise = new Promise(function(resolve, reject) {
    setTimeout(function() {
        const x = Math.random();
        console.log(x);
        if (x < 0.5)  {
            console.log('resolve');
            resolve();
        } else {
            console.log('reject');
            reject();
        }

    }, 1000);
});

myPromise
    .then(function() {
        console.log('success');
    })
    .catch(function() {
        console.log('fail');
    })
    .then(function() {
        console.log('complete');
    });

