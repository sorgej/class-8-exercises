const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';

const url = `${BASE_URL}?q=cars&api-key=${API_KEY}`;

fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);

    window.responseJson = responseJson;  //?

    let article = responseJson.response.docs[0];
    console.log(article);

    const mainHeadline = article.headline.main;
    document.getElementById('article-title').innerText = mainHeadline;
    
    if (article.multimedia.length > 0) {
      const imgUrl = `https://www.nytimes.com/${article.multimedia[0].url}`;
      document.getElementById('article-img').src = imgUrl;
    }

    const snippet = article.snippet;
    document.getElementById('article-snippet').innerText = snippet;

    const url = article.web_url;
    document.getElementById('article-link').href = url;

    const date = article.pub_date;

    var d = new Date(article.pub_date);
    var s = d.toLocaleString('en-US', {hour12: false,       timeZoneName: 'short'}).replace(',','');

    console.log(date);
    console.log(s);
    document.getElementById('article-date').innerText = `Publication date: ${s}`;


  });
