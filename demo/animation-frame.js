/**
 * Creates a new car
 * @constructor
 * @param {String} name - Name of car
 * @param {Number} maxSpeed - Max speed in MPH
 * @param {Number} accel - Acceleration, 0 to 25
 * @param {String} imgId - id of image element
 */
const Car = function(name, maxSpeed, accel, imgId){
  this.name = name;
  this.maxSpeed = maxSpeed;
  this.accel = accel;
  this.img = document.getElementById(imgId);
  this.leftPos = 0;
  this.remainingLaps = 6;
}
Car.prototype.move = function(x) {
  let mph = (this.maxSpeed * x ** 2) / ((30 - this.accel) + x ** 2);
  this.leftPos += (mph / 10);
  this.img.style.transform = `translate(${this.leftPos}px, -50%)`;
}

let cars = [new Car('Red car', 120, 15, 'car1'), new Car('Blue car', 140, 1, 'car2')];

const maxLeftPos = window.innerWidth;
const startTime = (new Date()).getTime();
let raceOver = false;

const animate = function() {
  const secondsPassed = ((new Date()).getTime() - startTime) / 1000;
  
  cars.forEach(function(car) {
    car.move(secondsPassed);

    if (car.leftPos > maxLeftPos) {
      car.remainingLaps--;
      if (car.remainingLaps <= 0) {
        raceOver = true;
        console.log(`${car.name} wins!`);
      }
      car.leftPos = -car.img.width;
    }
  })
  if (!raceOver) { requestAnimationFrame(animate); }
}
requestAnimationFrame(animate);